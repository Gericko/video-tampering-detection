import numpy as np
from math import sqrt
from scipy.stats import norm


def detect_outlier(
    horizontal_flow_sequence,
    vertical_flow_sequence,
    delta_t,
    std_dev_acc,
    significance_level=0.01,
    number_of_frames=30,
    verbose=False
):
    nb_moving_pixels = np.sum(np.logical_or(horizontal_flow_sequence != 0, vertical_flow_sequence != 0), axis=(1, 2))
    flow_tot = np.where(
        nb_moving_pixels == 0,
        np.zeros_like(nb_moving_pixels),
        np.sum(horizontal_flow_sequence + vertical_flow_sequence, axis=(1, 2)) / nb_moving_pixels
    )
    square_flow_tot = np.where(
        nb_moving_pixels == 0,
        np.zeros_like(nb_moving_pixels),
        np.sum(horizontal_flow_sequence ** 2 + vertical_flow_sequence ** 2, axis=(1, 2)) / nb_moving_pixels
    )

    tampered = False

    for t in range(1, len(square_flow_tot)):
        square_flow = square_flow_tot[t]
        square_flow_prec = square_flow_tot[t - 1]
        cardinal_pixel = nb_moving_pixels[t]
        flow_prec = flow_tot[t - 1]

        mean_no_tampering = delta_t ** 4 * std_dev_acc ** 2
        std_dev_no_tampering = \
            sqrt(2 / cardinal_pixel) * delta_t ** 4 * std_dev_acc ** 2 + \
            abs(flow_prec) * delta_t ** 2 * std_dev_acc ** 2
        variation_law_no_tampering = norm(mean_no_tampering, std_dev_no_tampering)

        mean_tampering = \
            (number_of_frames ** 2 - 1) * square_flow_prec + \
            delta_t ** 4 * std_dev_acc ** 2 * number_of_frames ** 2 * (number_of_frames - 1) ** 2 / 4
        std_dev_tampering = \
            sqrt(2 / cardinal_pixel) * delta_t ** 4 * std_dev_acc ** 2 * \
            number_of_frames ** 2 * (number_of_frames - 1) ** 2 / 4 + \
            abs(flow_prec) * delta_t ** 2 * std_dev_acc * number_of_frames ** 2 * (number_of_frames - 1) / 2
        variation_law_tampering = norm(mean_tampering, std_dev_tampering)

        critical_value = variation_law_tampering.ppf(significance_level)
        if verbose:
            print("Power of the test at frame {0}: {1}".format(t, variation_law_no_tampering.cdf(critical_value)))
        if square_flow - square_flow_prec > critical_value:
            tampered = True
            if verbose:
                print("Potential tempering detected at frame {}!".format(t))

    return tampered
