import numpy as np
from pyunlocbox import functions, solvers


def submatrix(mat, support):
    return mat[:, support]


def prune(vector, nb_elements):
    sorted_indices = np.argsort(vector)[-nb_elements::]  # create a copy of the vector containing the
    # nb_elements last indices
    support = np.full(vector.shape, False)
    support[sorted_indices] = True
    return support


def thresholding(vector, threshold):
    return np.abs(vector) >= threshold


def subset_max_energy(vector, percentage):
    vector_squared = vector ** 2
    energy_total = np.sum(vector_squared)
    sorted_indices = np.argsort(vector)[::-1]
    sum_energy = 0
    support = np.full(vector.shape, False)
    for i in range(len(vector_squared)):
        sum_energy += vector_squared[sorted_indices[i]]
        support[sorted_indices[i]] = True
        if sum_energy >= percentage * energy_total:
            break
    return support


class PracReProCS:

    def __init__(self, matrix_m, t_train=100, q=1, b=0.95, alpha=20, k_min=3, k_max=10):
        self.matrix_m = matrix_m
        self.t_train = t_train
        self.q = q
        self.b = b
        self.alpha = alpha
        self.k_min = k_min
        self.k_max = k_max
        self.n = matrix_m.shape[0]
        self.low_rank = None
        self.base = None
        self.sparse = None
        self.support = None

    def _approx_basis(self):
        training_matrix = self.matrix_m[:, :self.t_train]
        left_singular_matrix, diagonal, _ = self.svd(training_matrix)
        support = subset_max_energy(diagonal, self.b)
        return submatrix(left_singular_matrix, support), diagonal[support]

    def _least_square_constrained(self, vector, projector, support):
        sub_projector = submatrix(projector, support)
        sub_estimate = np.linalg.inv((sub_projector.T @ sub_projector)) @ (sub_projector.T @ vector)
        estimate = np.zeros(self.n)
        estimate[support] = sub_estimate
        return estimate

    @staticmethod
    def svd(matrix):
        return np.linalg.svd(matrix, full_matrices=False)

    def _initialization(self):
        initial_base, initial_singular_values = self._approx_basis()
        rank = np.linalg.matrix_rank(initial_base)
        singular_value_min = initial_singular_values[rank - 1]  # Check that svd puts the largest values first
        time_last_update = self.t_train
        base_reference = initial_base
        base_new_vectors = []
        k = 0
        detected = False
        self.sparse = np.zeros((self.n, self.t_train))
        self.support = np.full((self.n, self.t_train), False)
        self.low_rank = self.matrix_m[:, :self.t_train]
        self.base = []
        for i in range(self.t_train):
            self.base.append(initial_base.copy())
        return time_last_update, base_reference, base_new_vectors, k, detected, singular_value_min

    def _perpendicular_projection(self, vector, base):
        projector = np.identity(self.n) - base @ base.T
        projection = projector @ vector
        return projector, projection

    def _l1_minimization(self, t, projection, projector):
        f1 = functions.norm_l1()

        ksi = np.linalg.norm(projector @ self.low_rank[:, t - 1])
        f2 = functions.proj_b2(
            epsilon=ksi, y=projection, A=projector, tight=False, nu=np.linalg.norm(projector, ord=2) ** 2
        )

        initial_guess = self.matrix_m[:, t] - self.low_rank[:, t - 1]

        solver = solvers.douglas_rachford(step=1e-2)
        result = solvers.solve([f1, f2], initial_guess, solver, maxit=1000, verbosity='LOW')

        return result['sol']

    def _sparse_recovery(self, t):
        projector, projection = self._perpendicular_projection(self.matrix_m[:, t], self.base[t - 1])
        sparse_cs = self._l1_minimization(t, projection, projector)
        omega = self.q * np.std(self.matrix_m[:, t])
        self.support = np.column_stack((self.support, thresholding(sparse_cs, omega)))
        self.sparse = np.column_stack((
            self.sparse, self._least_square_constrained(projection, projector, self.support[:, t])
        ))

    def _low_rank_recovery(self, t):
        new_low_rank_vector = self.matrix_m[:, t] - self.sparse[:, t]
        self.low_rank = np.column_stack((self.low_rank, new_low_rank_vector))

    def _projection_stabilized(self, t, k, base_new_vectors):
        close_enough = False
        for i in range(k - 2, k):
            difference_projector = base_new_vectors[i - 1] @ base_new_vectors[i - 1].T - \
                                   base_new_vectors[i] @ base_new_vectors[i].T
            projector = base_new_vectors[i - 1] @ base_new_vectors[i - 1].T
            sum_diff_proj = np.zeros(self.n)
            sum_proj = np.zeros(self.n)
            close_enough = True
            for t_prime in range(t - self.alpha + 1, t + 1):
                sum_diff_proj += difference_projector @ self.low_rank[:, t_prime]
                sum_proj += projector @ self.low_rank[:, t_prime]
            if np.linalg.norm(sum_diff_proj) / np.linalg.norm(sum_proj) < 0.01:
                close_enough = False
                break
        return close_enough

    def _subspace_update(self, t, time_last_update, base_reference, base_new_vectors, k, detected, singular_value_min):
        if (t - time_last_update + 1) % self.alpha == 0:
            if not detected:
                last_low_rank = self.low_rank[:, t - self.alpha + 1: t + 1]
                low_rank_projected = (np.identity(self.n) - base_reference @ base_reference.T) @ \
                    last_low_rank / np.sqrt(self.alpha)
                _, diagonal, _ = self.svd(low_rank_projected)
                if np.any(diagonal > singular_value_min):
                    detected = True
                    time_last_update = t - self.alpha + 1
                    k = 0
                    base_new_vectors = []
                else:
                    self.base.append(self.base[-1])
            if detected:
                last_low_rank = self.low_rank[:, t - self.alpha + 1: t + 1]
                low_rank_projected = (np.identity(self.n) - base_reference @ base_reference.T) @ \
                    last_low_rank / np.sqrt(self.alpha)
                left_singular_matrix, diagonal, _ = self.svd(low_rank_projected)
                support_large_singular_vectors = thresholding(diagonal, singular_value_min)
                if support_large_singular_vectors.sum() > self.alpha:
                    support_large_singular_vectors = prune(diagonal, int(self.alpha / 3))
                base_new_vectors.append(left_singular_matrix[support_large_singular_vectors])
                k += 1
                self.base.append(base_new_vectors[-1])
                if k == self.k_max or k >= self.k_min and self._projection_stabilized(t, k, base_new_vectors):
                    base_reference = np.column_stack((base_reference, base_new_vectors[-1]))
                    detected = False
        else:
            self.base.append(self.base[-1])
        return time_last_update, base_reference, base_new_vectors, k, detected

    def fit(self, iter_print=20):
        time_last_update, base_reference, base_new_vectors, k, detected, singular_value_min = self._initialization()
        print("end of initialization at frame {}".format(self.t_train))
        for t in range(self.t_train, self.matrix_m.shape[1]):
            self._sparse_recovery(t)
            self._low_rank_recovery(t)
            time_last_update, base_reference, base_new_vectors, k, detected = self._subspace_update(
                t, time_last_update, base_reference, base_new_vectors, k, detected, singular_value_min
            )
            if (t - self.t_train) % iter_print == 0:
                print("frame: {0}, rank of base: {1}, cardinal of support: {2}".format(
                    t, np.linalg.matrix_rank(self.base[-1]), np.linalg.norm(self.support[:, -1].sum())
                ))
        return self.low_rank, self.sparse
