import numpy as np
from pyoptflow import HornSchunck


def optical_flow(video, alpha=0.1, n_iter=100):
    initialization_shape = (0,) + video.shape[1:]
    horizontal_flow_sequence = np.zeros(initialization_shape)
    vertical_flow_sequence = np.zeros(initialization_shape)
    for i in range(len(video) - 1):
        image1 = video[i]
        image2 = video[i + 1]
        horizontal_flow_vector, vertical_flow_vector = HornSchunck(image1, image2, alpha=alpha, Niter=n_iter)
        horizontal_flow_sequence = np.stack((horizontal_flow_sequence, horizontal_flow_vector))
        vertical_flow_sequence = np.stack((vertical_flow_sequence, vertical_flow_vector))
    return horizontal_flow_sequence, vertical_flow_sequence
