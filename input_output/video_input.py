from decord import VideoReader
import numpy as np


def srgb_to_linear(primary_srgb):
    primary_srgb = primary_srgb / 255.0
    primary_linear = (primary_srgb <= 0.04045) * primary_srgb / 12.92 + \
                     (primary_srgb > 0.04045) * ((primary_srgb + 0.055) / 1.055) ** 2.4
    return primary_linear


def linear_to_srgb(primary_linear):
    primary_srgb = (primary_linear <= 0.0031308) * 12.92 * primary_linear + \
                   (primary_linear > 0.0031308) * 1.005 * primary_linear ** (1 / 2.4) - 0.055
    primary_srgb = np.rint(primary_srgb * 255)
    return primary_srgb


def grayscale(video):
    shape = video.shape
    video_array = video.reshape(-1)
    video_linear_array = srgb_to_linear(video_array)
    video_linear = video_linear_array.reshape((-1, 3))
    video_gray_linear = np.matmul(video_linear, np.array([0.2126, 0.7152, 0.0722]))
    video_gray = linear_to_srgb(video_gray_linear)
    new_shape = shape[:-1]
    video_gray_final = video_gray.reshape(new_shape)
    return video_gray_final


def video_to_array(filename):
    vr = VideoReader(filename)
    all_indices = list(range(0, len(vr)))
    frames_color = vr.get_batch(all_indices).asnumpy()
    frames_gray = grayscale(frames_color)
    return frames_gray
