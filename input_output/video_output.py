import cv2
import numpy as np


def save_video(video_array, fps, filename):
    video_array_int = np.uint8(video_array)
    shape = (video_array_int.shape[2], video_array_int.shape[1])
    video = cv2.VideoWriter(filename, cv2.VideoWriter_fourcc(*'mp4v'), fps, shape, False)
    for frame_array in video_array_int:
        cv2.imshow("Frame", frame_array)
        video.write(frame_array)
    video.release()
