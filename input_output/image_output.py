import numpy as np
from keras.preprocessing.image import array_to_img


def show_array(image_array):
    if image_array.ndim == 2:
        image_array = image_array[..., np.newaxis]
    image = array_to_img(image_array)
    image.show()
