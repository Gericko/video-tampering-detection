import unittest
import numpy as np
import pracreprocs_ppca


class PracReProCSTest(unittest.TestCase):
    def test_prune(self):
        vector = np.array([4, 27, 8, 0, 6, 12])
        support = pracreprocs_ppca.prune(vector, 2)
        true_support = np.array([False, True, False, False, False, True])
        self.assertTrue(np.array_equal(support, true_support))

    def test_submatrix(self):
        matrix = np.array([[1, 2, 3],
                           [4, 5, 6]])
        support = np.array([False, True, True])
        matrix_true = np.array([[2, 3],
                                [5, 6]])
        matrix_result = pracreprocs_ppca.submatrix(matrix, support)
        self.assertTrue(np.array_equal(matrix_result, matrix_true))

    def test_subset_max_energy(self):
        vector = np.array([4, 5, 3])
        subset = pracreprocs_ppca.subset_max_energy(vector, 0.49)
        subset_true = np.array([False, True, False])
        self.assertTrue(np.array_equal(subset, subset_true))


if __name__ == '__main__':
    unittest.main()
