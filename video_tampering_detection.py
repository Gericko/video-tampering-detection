import cv2
import numpy as np
from input_output.video_input import video_to_array
from pracreprocs_ppca import PracReProCS
from pcp import pcp
from optical_flow import optical_flow
from outliers import detect_outlier


def tampering_detection(video_array, fps, number_of_frames):
    # video_array = video_to_array(video_filename)
    original_shape = video_array.shape
    matrix_shape = (original_shape[0], -1)
    video_matrix = video_array.reshape(matrix_shape).T

    pracreprocs_video = PracReProCS(video_matrix, q=0.25)
    # _, video_sparse_matrix = pracreprocs_video.fit()
    _, video_sparse_matrix, _, _ = pcp(video_matrix, factor=1, tol=10 ** (-7), maxit=1000)

    video_sparse = video_sparse_matrix.T.reshape(original_shape)
    video_foreground = (np.abs(video_sparse) > 20) * video_array

    horizontal_flow_sequence, vertical_flow_sequence = optical_flow(video_array)
    # horizontal_flow_sequence[np.logical_not(video_foreground_support)] = 0
    # vertical_flow_sequence[np.logical_not(video_foreground_support)] = 0

    cap = cv2.VideoCapture(video_filename)
    frames_per_second = int(cap.get(cv2.CAP_PROP_FPS))
    delta_t = 1 / fps
    max_size = max(original_shape[1:])
    std_dev_acc = max_size / 3
    tampered = detect_outlier(horizontal_flow_sequence, vertical_flow_sequence, delta_t, std_dev_acc, number_of_frames=number_of_frames)

    return tampered
