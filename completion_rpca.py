import numpy as np


class CompletionRpca:

    def __init__(self, D, support, mu=None, lmbda=None, tol=None):
        self.D = D
        self.support = support
        self.support_ortho = np.logical_not(support)
        self.L = np.zeros(self.D.shape)
        self.S = np.zeros(self.D.shape)
        self.E = np.zeros(self.D.shape)
        self.Y = np.zeros(self.D.shape)

        if mu:
            self.mu = mu
        else:
            self.mu = np.prod(self.D.shape) / (4 * np.linalg.norm(self.D, ord=1))
        self.mu_inv = 1 / self.mu

        if lmbda:
            self.lmbda = lmbda
        else:
            self.lmbda = 1 / np.sqrt(np.max(self.D.shape))

        if tol:
            self.tol = tol
        else:
            self.tol = 1E-7 * self.frobenius_norm(self.D)

    @staticmethod
    def frobenius_norm(M):
        return np.linalg.norm(M, ord='fro')

    @staticmethod
    def shrink(M, tau):
        return np.sign(M) * np.maximum((np.abs(M) - tau), np.zeros(M.shape))

    def svd_threshold(self, M, tau):
        U, S, V = np.linalg.svd(M, full_matrices=False)
        return U @ np.diag(self.shrink(S, tau)) @ V

    def projection(self, E):
        E[self.support_ortho] = 0
        return E

    def fit(self, max_iter=1000, iter_print=100):
        iter = 0
        err = np.Inf
        Lk = self.L
        Sk = self.S
        Ek = self.E
        Yk = self.Y

        while err > self.tol and iter < max_iter:
            Lk = self.svd_threshold(self.D - Ek - Sk + self.mu_inv * Yk, self.mu_inv)
            Ek = self.projection(self.D - Lk - Sk + self.mu_inv * Yk)
            Sk = self.shrink(self.D - Lk - Ek + self.mu_inv * Yk, self.mu_inv * self.lmbda)
            Yk = Yk + self.mu * (self.D - Lk - Ek - Sk)
            err = self.frobenius_norm(self.D - Lk - Ek - Sk)
            iter += 1
            if (iter % iter_print) == 0 or iter == 1 or iter >= max_iter or err <= self.tol:
                print('iteration: {0}, error: {1}'.format(iter, err))

        self.L = Lk
        self.E = Ek
        self.S = Sk
        return Lk, Sk
